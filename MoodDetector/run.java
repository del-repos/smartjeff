//=================================================================
// Tudor Mironovici        tcm26@njit.edu
//
// Purpose: read the temp .txt file and retrieve the emotion info.
//=================================================================
import java.util.*;
import java.io.*;
public class run
{
   public static void main(String[] args) throws FileNotFoundException, IllegalArgumentException, IOException
   {
      File file = new File("C:\\Users\\Satellite\\Desktop\\yeye.txt");
      Scanner scan = new Scanner(file);
      BufferedWriter writer = new BufferedWriter(new FileWriter("C:\\Users\\Satellite\\export.json"));
      String currentLine;
      int line = 1;
      
      while (scan.hasNextLine())
      {
         currentLine = scan.nextLine();
         
         if (line == 110)
         {
            for (int i = 0; i < 9; i++)
            {
               for (int c = 0; c < currentLine.length(); c++)
               {
                  if (currentLine.charAt(c) != ' ')
                  {
                     writer.write(currentLine.charAt(c));
                  }
               }
               currentLine = scan.nextLine();
               writer.write("\n");
            }
            writer.write("}");
            break;
         }
         
         line += 1;
      }
      writer.close();
      
      
      file = new File("C:\\Users\\Satellite\\export.json");
      scan = new Scanner(file);
      String[] currParsed;
      String[] emotions = new String[8];
      double[] eValues = new double[8];
      String best_emotion = "";
      currentLine = scan.nextLine();
      
      for (int cnt = 0; cnt < 8; cnt++)
      {
         currentLine = scan.nextLine();
         currParsed = currentLine.split(":");
         
         emotions[cnt] = currParsed[0].substring(1, currParsed[0].length() - 1);
         
         if (cnt != 7)
         {
            eValues[cnt] = Double.parseDouble(currParsed[1].substring(0, currParsed[1].length() - 1));
         }
         
         else
         {
            eValues[cnt] = Double.parseDouble(currParsed[1]);
         }
      }
      
      double best = 0.0;
      
      for (int j = 0; j < 8; j++)
      {
         if (eValues[j] > best)
         {
            best = eValues[j];
            best_emotion = emotions[j]; 
         }
      }
      
      writer = new BufferedWriter(new FileWriter("C:\\Users\\Satellite\\emotion.txt"));
      writer.write(best_emotion);
      writer.close();
   }
}
from flask import Flask, request, render_template, json
from azure_mood import appMod
from twilio.twiml.messaging_response import MessagingResponse
import random
from geolocation.main import GoogleMaps
from geolocation.distance_matrix.client import DistanceMatrixApiClient
import requests
import json as jsonB


app = Flask(__name__)


def get_lat_lon():
    send_url = 'http://freegeoip.net/json'
    r = requests.get(send_url)
    j = jsonB.loads(r.text)
    lat = j['latitude']
    lon = j['longitude']
    return [lat, lon]


google_maps = GoogleMaps(api_key='AIzaSyCLp-P6MuktEFOEdvGpiyzClmDUjCZjwCU')
print(google_maps.search(lat=get_lat_lon()[0],lng=get_lat_lon()[1]))


@app.route("/report/", methods=['GET','POST'])


def take_json(emotions):
    outgoing_sms(appMod.best_emotion(emotion=emotions))

@app.route("/sms", methods=['GET', 'POST'])



def incoming_sms():
    """Send a dynamic reply to an incoming text message"""
    # Get the message the user sent our Twilio number
    body = request.values.get('Body', None)

    # Start our TwiML response
    resp = MessagingResponse()


    # Determine the right reply for this message
    if body == 'Hello':
        resp.message("Hi!")
    elif body == 'bye':
        resp.message("Goodbye")
    else:
        resp.message("What up homeslice!")

    return str(resp)


def outgoing_sms(emotion):
    resp = MessagingResponse()

    anger = list(['burgers'])
    fear = list(['bread'])
    happiness = list(['pizza','steak'])
    neutral = list(['pizza, fruit'])
    sadness = list(['ice cream', 'desserts'])

    default = list(['pizza'])

    if emotion == 'anger':
        resp.message("Mood: {}\nRecommendation: {}\n".format(emotion, random.randint(0, len(anger))))
    elif emotion == 'fear':
        resp.message("Mood: {}\nRecommendation: {}\n".format(emotion,random.randint(0,len(fear))))
    elif emotion == 'happiness':
        resp.message("Mood: {}\nRecommendation: {}\n".format(emotion,random.randint(0,len(happiness))))
    elif emotion == 'neutral':
        resp.message("Mood: {}\nRecommendation: {}\n".format(emotion,random.randint(0,len(neutral))))
    elif emotion == 'sadness':
        resp.message("Mood: {}\nRecommendation: {}\n".format(emotion, random.randint(0,len(sadness))))
    else:
        resp.message("Mood: {}\nRecommendation: {}\n".format(emotion, random.randint(0,len(default))))

    return str(resp)


if __name__ == "__main__":
    app.run(debug=True)